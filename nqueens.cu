#include<iostream>



#define N 12

using namespace std;


__device__ __host__ bool check_pos(int row, int col, int *q_col)
{
    for(int i=0; i<row; i++)
    {
        if(q_col[i] == col)
        {
            return false;
        }
        if(abs(i - row) == abs(q_col[i] - col))
        {
            return false;
        }
    }
    return true;
}

__global__ void nqueens_gpu(int row, int *q_col)
{
    if(row == N)
    {
        return true;
    }
    for(int i=0; i<N; i++)
    {
        if(check_pos(row, i, q_col))
        {
            q_col[row] = i;
            if(nqueens(row+1, q_col))
            {
                return true;
            }   
            else
            {
                q_col[row] = -1;
            }
        }
    }
    return false;
}


int main()
{

}