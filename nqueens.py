import math
n=5
m = [0]*n
q_col = [None]*n
for i in range(n):
    m[i] = [0]*n


def print_board():
    for row in m:
        print(row)

def check(row, col):
    for i in range(row):
        if q_col[i] == col:
            return False
        if abs(i - row) == abs(q_col[i]-col):
            return False
    return True
    
    pass

def nqueens(row):
    if row == n:
        return True
    
    for i in range(n):
        if check(row, i):
            m[row][i] = 1
            q_col[row] = i
            if nqueens(row+1):
                return True
            else:
                m[row][i] = 0
                q_col[row] = -1
    return False



nqueens(0)

print_board()
    

