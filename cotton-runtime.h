#ifndef COTTON_RUNTIME
#define COTTON_RUNTIME

#include<pthread.h>
#include<functional>

// * Class declarations

class Task;
class TaskDequeue;
class Worker;
class CottonThreadPool;

using namespace std;

class Task
{
    private:
    function<void*(void*)>job;
    void *vargs;


    public:
    Task(function<void*(void*)> job, void *vargs);
    function<void*(void *)> getJob();
    void setJob(function<void*(void*)> job);
    void *getVargs();
    void setVargs(void *vargs);

    void *run();
};

class TaskDequeue
{
    private:
    volatile int capacity;
    volatile int head;
    volatile int tail;
    Task **store;
    pthread_mutex_t lock;

    void expand();
    int nextHead();
    int prevHead();
    int nextTail();
    int prevTail();
    Task *pop_async();
    Task *steal_async();

    public:

    TaskDequeue(int capacity=2<<14);
    ~TaskDequeue();
    void push(Task *t);
    Task *pop();
    Task *steal();
    bool isEmpty();
    bool isFull();
    Task **getStore();
    int getCapacity();

};

class Worker
{
    private:
    int id;
    TaskDequeue dq;
    pthread_t thread_id;
    CottonThreadPool *masterPool;
    volatile bool keep_alive;
    static void *worker_routine(void *);
    void initiateShutdown();

    public:
    Worker(int id, CottonThreadPool *masterPool);
    Task *stealWork();
    CottonThreadPool *getMasterPool();
    void destroyWorker();
    TaskDequeue *getDq();
    void initWorker();
    ~Worker();    
};

class CottonThreadPool
{
    private:
    int num_workers;
    volatile long finish_counter;
    Worker **workers;
    pthread_mutex_t finish_lock;

    public:
    CottonThreadPool(int num_workers); //* init
    void async(function<void*(void*)> lambda, void *vargs);
    void start_finish();
    void end_finish();
    ~CottonThreadPool(); //* finalise
    int getNumWorkers();
    Worker **getWorkers();
    void lock_finish();
    void unlock_finish();
    void decrement_finish_counter();
    void increment_finish_counter();

};











#endif