#include<iostream>
#include<pthread.h>
#include<functional>
#include"cotton-runtime.h"
#include<stdio.h>
#include<cstdlib>
#define N 4

#define DIM 100

using namespace std;


bool check_pos(int row, int col, int *q_col)
{
    for(int i=0; i<row; i++)
    {
        if(q_col[i] == col)
        {
            return false;
        }
        if(abs(i - row) == abs(q_col[i] - col))
        {
            return false;
        }
    }
    return true;
}

bool nqueens(int row, int q_col[N])
{
    if(row == N)
    {
        return true;
    }
    for(int i=0; i<N; i++)
    {
        if(check_pos(row, i, q_col))
        {
            q_col[row] = i;
            if(nqueens(row+1, q_col))
            {
                return true;
            }   
            else
            {
                q_col[row] = -1;
            }
        }
    }
    return false;
}

void display_board(int *q_col)
{
    for(int i=0; i<N; i++)
    {
        for(int j=0; j<N; j++)
        {
            if(q_col[i]==j)
            {
                cout<<"1 ";
            }
            else
            {
                cout<<"0 ";
            }
        }
        cout<<endl;
    }
}

int main()
{
    srand(0);
    CottonThreadPool pool(8);
    cout<<"ThreadPool created successfully!\n";
    int mat1[DIM][DIM];
 
    int mat2[DIM][DIM];

    for(int i=0; i<DIM; i++)
    {
        for(int j=0; j<DIM; j++)
        {
            mat1[i][j] = rand()%DIM;
            mat2[i][j] = rand()%DIM;
        }
    }

    
    pool.start_finish();
    int rslt[DIM][DIM]; 
    
    for (int i = 0; i < DIM; i++) 
    {
        for (int j = 0; j < DIM; j++) 
        {
            rslt[i][j] = 0;

            pool.async([&rslt, mat1, mat2, i, j] (void *vargs) -> void*
            {    
                for (int k = 0; k < DIM; k++) 
                {
                    rslt[i][j] += mat1[i][k] * mat2[k][j];
                }
                return vargs;
            }, NULL);
 
            
        }
 
        // cout << endl;
    }

    pool.end_finish();
    cout<<"Finish Ended!\n";


    // cout<<"Result: "<<arg_arr[1]<<endl;

    // int *q_col = (int*)malloc(N*sizeof(int));
    // nqueens(0, q_col);
    // display_board(q_col);
    // free(q_col);




}
