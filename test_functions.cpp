#include<functional>
#include<iostream>

using namespace std;


int f(int x)
{
    return x-1;
}

int lambda_acceptor(function<void*(void*)> fun)
{
    int a = 10;
    return *((int *)fun((void*)&a));
}

int main()
{
    function<void*(void*)> job;
    job = [](void *vargs) -> void* 
    {
        int *a = (int *)vargs;
        *a = (*a)*(*a);
        return a;
    };
    cout << lambda_acceptor(job) <<endl;

}