#include"cotton-runtime.h"
#include<stdlib.h>
#include<pthread.h>
#include<errno.h>
#include<string.h>
#include<stdio.h>
#include<iostream>
#include<functional>
#include<time.h>

int check(int error_code, const char* msg)
{
    if(error_code < 0)
    {
        perror(msg);
        exit(EXIT_FAILURE);
    }
    else
    {
        return error_code;
    }
}

int p_check(int error_code, const char* msg)
{
    if(error_code == 0)
    {
        return 0;
    }
    else
    {
        printf("%s\n%s\n", msg, strerror(error_code));
        exit(EXIT_FAILURE);
    }
}

int randint(int high)
{
    return rand()%high;
}

Task **TaskDequeue::getStore()
{
    return store;
}

TaskDequeue::TaskDequeue(int capacity)
{
    this->capacity = capacity;
    store = (Task**)malloc(capacity*sizeof(Task*));
    head = 0;
    tail = 0;
    // size = 0; // * Updating size introduces the need for synchronisation in push()
    // * Initialising Mutex Lock
    p_check(pthread_mutex_init(&lock, NULL), "Error Initialising Mutex for TaskDequeue!");




}

int TaskDequeue::nextHead()
{
    return (head+1)%capacity;
}

int TaskDequeue::prevHead()
{
    return (head-1+capacity)%capacity;
}

int TaskDequeue::nextTail()
{
    return (tail-1+capacity)%capacity;
}

int TaskDequeue::prevTail()
{
    return (tail+1)%capacity;
}

bool TaskDequeue::isEmpty()
{
    return (head == tail);
}

int TaskDequeue::getCapacity()
{
    return capacity;
}

bool TaskDequeue::isFull()
{
    return (nextHead() == tail);
}

void TaskDequeue::expand()
{
    // * Need to lock the queue before expanding

    p_check(pthread_mutex_lock(&lock), "Error Acquiring Mutex Lock in expand()!");
    TaskDequeue temp(2*capacity);
    while(!isEmpty())
    {
        temp.push(pop_async());
    }
    
    this->head = temp.head;
    this->tail = temp.tail;

    Task **tempStore = temp.getStore();
    temp.store = this->store;
    this->store = tempStore;
    
    this->capacity *= 2;



    p_check(pthread_mutex_unlock(&lock), "Error Unlocking Mutex in expand()!");
    // * Exapnsion done!
}


void TaskDequeue::push(Task *t)
{
    // cout<<"Tail: "<<tail<<" Head: "<<head<<" Empty:"<<isEmpty()<<endl;
    // * If the queue is not full
    if(!isFull())
    {
        // p_check(pthread_mutex_lock(&lock), "Error Acquiring Mutex Lock in push()!");
        // * decrement tail
        tail = nextTail();
        store[tail] = t;
        // std::cout<<"Tail:"<<tail;
        // p_check(pthread_mutex_unlock(&lock), "Error Releasing Mutex Lock in push()!");
    }        
    else // * if the queue is full, expand the queue
    {
        expand();
        push(t);
    }
}

Task *TaskDequeue::pop_async()
{
    if(!isEmpty())
    {
        Task *temp = store[tail];
        store[tail] = NULL;
        tail = prevTail();
        return temp;
    }
    else
    {
        return NULL;
    }
}

Task *TaskDequeue::pop()
{
    p_check(pthread_mutex_lock(&lock), "Error Acquiring Mutex Lock in pop()!");
    Task* temp = pop_async();
    p_check(pthread_mutex_unlock(&lock), "Error Unlocking Mutex in pop()!");
    return temp;
}



Task *TaskDequeue::steal_async()
{
    if(!isEmpty())
    {
        head = prevHead();
        Task *temp = store[head];
        store[head] = NULL;
        // size--;
        return temp;
    }
    else
    {
        return NULL;
    }
}



Task *TaskDequeue::steal()
{
    p_check(pthread_mutex_lock(&lock), "Error Acquiring Mutex Lock in steal()!");
    Task* temp = steal_async();
    p_check(pthread_mutex_unlock(&lock), "Error Unlocking Mutex in  steal()!");
    return temp;
}


TaskDequeue::~TaskDequeue()
{
    // cout<<"DQ Deallocated\n";
    free(store);
    store=NULL;
    // * Destroying Mutex Lock
    p_check(pthread_mutex_destroy(&lock), "Error Destroying Mutex");
}


// * class Task function definitions

Task::Task(function<void*(void*)> job, void *vargs)
{
    this->job = job;
    this->vargs = vargs;
}

function<void*(void *)> Task::getJob()
{
    return job;
}

void Task::setJob(function<void*(void*)> job)
{
    this->job = job;
}

void *Task::getVargs()
{
    return vargs;
}
void Task::setVargs(void *vargs)
{
    this->vargs = vargs;
}

void* Task::run()
{
    // TODO: Store result and completion status
    return job(vargs);
}

// * class Worker function definitions

Worker::Worker(int id, CottonThreadPool *masterPool)
{
    this->id = id;
    this->masterPool = masterPool;
    keep_alive = true;
    // initWorker();

}


void Worker::initWorker()
{
    p_check(pthread_create(&thread_id, NULL, Worker::worker_routine, (void*)this), "Error launching worker thread");
}

Task *Worker::stealWork()
{
    int index;
    do
    {
        index = randint(masterPool->getNumWorkers());
    }while(index == id && masterPool->getNumWorkers() > 1);
    TaskDequeue *randDq = masterPool->getWorkers()[index]->getDq();
    // debug
    // if(!randDq->isEmpty())
    //     cout<<"Steal!"<<endl;
    // debug
    return randDq->steal(); 
}

CottonThreadPool *Worker::getMasterPool()
{
    return masterPool;
}

void Worker::initiateShutdown()
{
    keep_alive = false;
}

void Worker::destroyWorker()
{
    initiateShutdown();
    p_check(pthread_join(thread_id, NULL), "Error joining threads!");
}

Worker::~Worker()
{
    cout<<"Worker "<<id<< " Destructor!\n";
}

TaskDequeue* Worker::getDq()
{
    return &dq;
}

// * Worker Routine Kernel

void* Worker::worker_routine(void *vargs)
{
    Worker *self = (Worker*)vargs;
    CottonThreadPool *masterPool = self->getMasterPool();
    // todo: manage results
    Task *nextJob = NULL;
    while(self->keep_alive == true) // todo: change this
    {
        nextJob = self->dq.pop();
        if(nextJob != NULL)
        {
            // cout<<"Pop!\n";
            // cout<<self->id<<"Self dq Non Empty!\n";
            nextJob->run();
            delete nextJob;
            nextJob = NULL;
            masterPool->decrement_finish_counter();
        }
        else
        {
            // cout<<self->id<<"Self dq Empty!\n";
            nextJob = self->stealWork();
            if(nextJob != NULL)
            {
                nextJob->run();
                delete nextJob;
                nextJob = NULL;
                masterPool->decrement_finish_counter();
            }
            // cout<<"trying again!\n";
        }
    }


    return NULL;

}

// * class CottonThreadPool function definitions

CottonThreadPool::CottonThreadPool(int num_workers)
{
    this->finish_counter = 0;
    p_check(pthread_mutex_init(&finish_lock, NULL), "Error init Finish Lock!");
    this->num_workers = num_workers;
    this->workers = (Worker**)malloc(num_workers*sizeof(Worker*));
    for(int i=0; i<num_workers; i++)
    {
        // cout<<"worker "<<i<<" will be created"<<endl;
        workers[i] = new Worker(i, this);
        cout<<"worker "<<i<<" created"<<endl;
    }
    for(int i=0; i<num_workers; i++)
    {
        workers[i]->initWorker();
        // cout<<"Worker "<<i<<"launched"<<endl;
    }
    // * Workers initialised
}

void CottonThreadPool::lock_finish()
{
    p_check(pthread_mutex_lock(&finish_lock), "Error locking finish mutex!");
}

void CottonThreadPool::unlock_finish()
{
    p_check(pthread_mutex_unlock(&finish_lock), "Error unlocking finish mutex!");
}


void CottonThreadPool::async(function<void*(void*)> lambda, void *vargs)
{
    increment_finish_counter();
    Task *newTask = new Task(lambda, vargs);
    // * modified
    workers[0]->getDq()->push(newTask);
}

void CottonThreadPool::start_finish()
{
    lock_finish();
    finish_counter=0;
    unlock_finish();
}

void CottonThreadPool::end_finish()
{
    // todo: replace this with a more efficient approach
    
    while(finish_counter != 0)
    {
        cout<<finish_counter<<endl;
        for(int i=0; i<num_workers; i++)
        {
            cout<<workers[i]->getDq()->isEmpty()<<"\t";
        }
        cout<<endl;
    }
}

CottonThreadPool::~CottonThreadPool()
{
    // end_finish();
    for(int i=0; i<num_workers; i++)
    {
        workers[i]->destroyWorker();
    }
    for(int i=0; i<num_workers; i++)
    {
        delete workers[i];
        workers[i] = NULL;
    }
    free(workers);
    workers = NULL;
    pthread_mutex_destroy(&finish_lock);
}

int CottonThreadPool::getNumWorkers()
{
    return num_workers;
}

Worker** CottonThreadPool::getWorkers()
{
    return workers;
}
void CottonThreadPool::decrement_finish_counter()
{
    lock_finish();
    finish_counter--;
    unlock_finish();
}

void CottonThreadPool::increment_finish_counter()
{
    lock_finish();
    finish_counter++;
    unlock_finish();
}