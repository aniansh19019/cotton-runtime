nqueens: nqueens.cpp cotton-runtime.o
	g++ -o nqueens -pthread nqueens.cpp cotton-runtime.o

cotton-runtime.o: cotton-runtime.cpp
	g++ -o cotton-runtime.o -c cotton-runtime.cpp

clean:
	rm -f nqueens cotton-runtime.o